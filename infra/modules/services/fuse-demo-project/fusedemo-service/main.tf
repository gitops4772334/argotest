terraform {
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.3"
    }
  }
}

provider "kubernetes" {
  config_path      = var.cluster_config_filename
  load_config_file = true
}

provider "kubectl" {
  config_path      = var.cluster_config_filename
  load_config_file = true
}

resource "null_resource" "argocd_id" {
  triggers = {
    dependency = "${var.argocd_id}"
  }
}

data "kubectl_file_documents" "argocd_application_fuse_demo" {
  content = file("/k8s-test/argotest/infra/argocd-confs/fuse-demo-project/fusedemo-service/fusedemo-app.yaml")
}

resource "kubectl_manifest" "argocd" {
  count      = length(data.kubectl_file_documents.argocd_application_fuse_demo.documents)
  yaml_body  = element(data.kubectl_file_documents.argocd_application_fuse_demo.documents, count.index)
  depends_on = [ null_resource.argocd_id ]
}