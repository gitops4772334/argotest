variable "cluster_config_filename" {
  description = "Cluster config file"
  type        = string
}

variable "argocd_id" {
  description = "ArgoCD id"
  type        = string
  default = ""
}