output "kubernetes_cluster_config" {
  description = "kubernetes cluster config file"
  value = local_sensitive_file.kube_cluster_yaml.filename
}