terraform {
  required_providers {
    rke = {
      source = "rancher/rke"
      version = "1.4.2"
    }
  }
}


resource "rke_cluster" "cluster" {
    nodes {
        address = "emsa-srv07"
        user    = "kubernetes"
        role    = ["controlplane", "worker", "etcd"]
        ssh_key = file("~/.ssh/id_rsa")
  }
  nodes {
        address = "emsa-srv08"
        user    = "kubernetes"
        role    = ["worker"]
        ssh_key = file("~/.ssh/id_rsa")
  }
  
  ingress {
    provider = "nginx"
  }
  enable_cri_dockerd = true
}

resource "local_sensitive_file" "kube_cluster_yaml" {
  filename = "./kube_config_cluster.yml"
  content  = "${rke_cluster.cluster.kube_config_yaml}"

  depends_on = [
    rke_cluster.cluster
  ]
}