output "id" {
  description = "argocd id"
  value = "${helm_release.argocd.id}"
}