terraform {
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.3"
    }
  }
}

provider "helm" {
  kubernetes {
    config_path = var.cluster_config_filename
  }
}

provider "kubernetes" {
  config_path      = var.cluster_config_filename
  load_config_file = true
}

provider "kubectl" {
  config_path      = var.cluster_config_filename
  load_config_file = true
}

resource "helm_release" "argocd" {
  name  = "argocd"

  repository       = "https://argoproj.github.io/argo-helm"
  chart            = "argo-cd"
  namespace        = "argocd"
  version          = "4.9.7"
  create_namespace = true

}