module "rke_cluster" {
  source = "./modules/rke_cluster"
}

module "argocd" {
  source = "./modules/argocd"
  cluster_config_filename = module.rke_cluster.kubernetes_cluster_config
}

module "fuse_service" {
  source = "./modules/services/fuse-demo-project/fusedemo-service"
  cluster_config_filename = module.rke_cluster.kubernetes_cluster_config
  argocd_id = "${module.argocd.id}"
}