package com.example.demo.rest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Description;

@RestController
@RequestMapping
public class BaseController {
	
	@GetMapping(path = "/{text}")
    public Description findBaseTemplates(@PathVariable(name = "text") String text) {
        return new Description(text);
    }
}
